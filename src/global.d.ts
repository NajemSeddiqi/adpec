export {};

declare global {
  interface Date {
    getLongMonthName(): string;
    getShortMonthName(): string;
    getAmPmTime(): string;
  }

  interface Array<T> {
    asDictionary<TArray, TValue, TKey extends number | string | symbol>(
      this: TArray[],
      getValue: (row: TArray) => TValue,
      getKey: (row: TArray) => TKey,
      createValue: (record: Record<TKey, TValue>, key: TKey, value: TValue) => void,
      appendValue: (record: Record<TKey, TValue>, key: TKey, value: TValue) => void,
    ): [keys: TKey[], values: TValue[]];
  }
}
