import { Component, OnInit } from '@angular/core';
import { ToastService, ToastType } from './services/toast/toast.service';
import { tap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'AdPec';
  toastMessage?: string = "";
  toastClass: string = "toast collapse";

  private toastInterval: any;

  constructor(private toaster: ToastService) {
  }

  ngOnInit(): void {
    this.toaster.displayToast$.pipe(tap(next => {
      this.toastMessage = next?.message;
      if(next?.toastType !== undefined) this.toastClass = this.getToastClass(next?.toastType);

      clearInterval(this.toastInterval);

      this.toastInterval = setInterval(() => {
        this.toastClass = "toast collapse";
        clearInterval(this.toastInterval);
      }, 3500);

    })).subscribe();
  }

  private getToastClass(toastType: ToastType):string {
    switch(toastType) {
      case ToastType.Success:
        return "toast success slide-in";
      case ToastType.Info:
        return "toast info slide-in";
      case ToastType.Warning:
        return "toast warning slide-in";
      case ToastType.Error:
        return "toast error slide-in";
      default:
        return "unknown";
    }
  }
}
