import { Routes } from '@angular/router';
import { MyAdpecComponent } from './components/myAdPec/my-adpec/my-adpec.component';
import { PurchasesComponent } from './components/purchases/purchases/purchases.component';
import { BankAndCardComponent } from './components/bankAndCard/bank-and-card/bank-and-card.component';
import { SavedComponent } from './components/saved/saved/saved.component';
import { InspirationComponent } from './components/inspiration/inspiration/inspiration.component';
import { PurchaseComponent } from './components/purchase/purchase.component';
import { DeliveredComponent } from './components/delivered/delivered.component';
import { EmissionsComponent } from './components/emissions/emissions.component';
import { PaymentsComponent } from './components/payments/payments/payments.component';

export const routes: Routes = [
  {path: '', redirectTo: '/adPec', pathMatch: 'full'},
  {path: 'adPec', component: MyAdpecComponent},
  {path: 'purchases', component: PurchasesComponent},
  {path: 'bankAndCard', component: BankAndCardComponent},
  {path: 'saved', component: SavedComponent},
  {path: 'inspiration', component: InspirationComponent},
  {path: 'purchase/:id', component: PurchaseComponent},
  {path: 'delivered/:id', component: DeliveredComponent},
  {path: 'emissions', component: EmissionsComponent},
  {path: 'payments', component: PaymentsComponent},
];
