export {};

Date.prototype.getLongMonthName = function(): string {
  return this.toLocaleString('default', { month: 'long' });
}

Date.prototype.getShortMonthName = function(): string {
  return this.toLocaleString('default', { month: 'short' });
}

Date.prototype.getAmPmTime = function(): string {
  return this.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
}

Array.prototype.asDictionary = function<TArray, TValue, TKey extends number | string | symbol>(
  this: TArray[],
  getValue: (row: TArray) => TValue,
  getKey: (row: TArray) => TKey,
  createValue: (record: Record<TKey, TValue>, key: TKey, value: TValue) => void,
  appendValue: (record: Record<TKey, TValue>, key: TKey, value: TValue) => void
): [keys: TKey[], values: TValue[]] {

  let valuesByKeys = {} as Record<TKey, TValue>;

  this.forEach(row => {
    const value = getValue(row);
    const key = getKey(row);

    if(valuesByKeys[key]) appendValue(valuesByKeys, key, value);
    else createValue(valuesByKeys, key, value);
  });

  const keys = Object.keys(valuesByKeys).map(key => key) as TKey[];
  const values = Object.keys(valuesByKeys).map(key => valuesByKeys[key as TKey]);

  return [keys, values];
};
