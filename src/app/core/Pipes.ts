import { Pipe, PipeTransform } from '@angular/core';
import { InvoiceProduct } from '../models/Invoice/InvoiceProduct';

@Pipe({ name: 'capitalize' })
export class CapitalizePipe implements PipeTransform {

  transform(value: string | undefined, ...args: string[]): string {
    if (!value) return "String must not be empty.";
    return value.charAt(0).toUpperCase() + value.slice(1);
  }

}

@Pipe({ name: 'replaceAndWithSymbol' })
export class ReplaceAndWithSymbolPipe implements PipeTransform {

  transform(value: string | undefined, ...args: string[]): string {
    if (!value) return "String must not be empty.";
    if(!value.includes("And")) return value;

    let replacedAndWithSymbol: string = '';

    const and = 'And';
    const startIndex = value.indexOf(and);

    if (startIndex !== -1) {
      const endIndex = startIndex + and.length - 1;
      if(endIndex !== value.length - 1) {
          const prefix = value.substring(0, startIndex);
          const suffix = value.substring(endIndex + 1, value.length);
          replacedAndWithSymbol = prefix + " & " + suffix;
      }
    }

    return replacedAndWithSymbol;
  }

}

@Pipe({name: 'sumProducts'})
export class SumProducts implements PipeTransform {

  transform(value: InvoiceProduct[] | undefined, ...args: InvoiceProduct[] | undefined[]): number {
      if(!value) return -1;
      if(value.length < 1) return -1;
      const sum = value.reduce((sum: number, current: InvoiceProduct) => sum + current.sum, 0);
      return sum;
  }

}

@Pipe({name: 'paymentDueInDays'})
export class PaymentDueInDays implements PipeTransform {

  transform(dueDate: Date | undefined, purchaseDate: Date | undefined): string {
    if(!dueDate || !purchaseDate) return "The dates cannot be undefined."
    const difference = Math.abs(dueDate.getTime() - purchaseDate.getTime());
    const differenceInDays = Math.ceil(difference / (1000 * 60 * 60 * 24));
    return `Payment due in ${differenceInDays} days`
  }

}


@Pipe({name: 'prettyfyHyperlink'})
export class PrettyfyHyperlink implements PipeTransform {
  regex = /^https?:\/\/(www\.)?/;

  transform(value: string | undefined, ...args: string[]): string {
    if(value === "" || !value) return "";

    return value.replace(this.regex, "");
  }

}

