import { Subscription } from "rxjs";

export class DisposableComponentBase {

  private disposables: Subscription[] = [];

  registerDisposable(disposable: Subscription) {
    this.disposables.push(disposable);
  }

  dispose() {
    this.disposables.forEach(disposable => disposable.unsubscribe());
  }

}
