import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar/navbar.component';
import { PurchasesComponent } from './components/purchases/purchases/purchases.component';
import { BankAndCardComponent } from './components/bankAndCard/bank-and-card/bank-and-card.component';
import { MyAdpecComponent } from './components/myAdPec/my-adpec/my-adpec.component';
import { SavedComponent } from './components/saved/saved/saved.component';
import { InspirationComponent } from './components/inspiration/inspiration/inspiration.component';

import { routes } from '../app/routes';
import { RouterModule } from '@angular/router';
import { CapitalizePipe, PaymentDueInDays, PrettyfyHyperlink, ReplaceAndWithSymbolPipe, SumProducts } from './core/Pipes';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { PurchaseComponent } from './components/purchase/purchase.component';
import { DeliveredComponent } from './components/delivered/delivered.component';
import { ChangeCategoryComponent } from './components/change-category/change-category.component';
import { EmissionsComponent } from './components/emissions/emissions.component';
import { ColumnChartComponent } from './components/column-chart/column-chart.component';
import { InspirationSliderComponent } from './components/inspirationSlider/inspiration-slider/inspiration-slider.component';
import { PaymentsComponent } from './components/payments/payments/payments.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PurchasesComponent,
    BankAndCardComponent,
    MyAdpecComponent,
    SavedComponent,
    InspirationComponent,
    SearchBarComponent,
    CapitalizePipe,
    ReplaceAndWithSymbolPipe,
    PrettyfyHyperlink,
    SumProducts,
    PaymentDueInDays,
    SpinnerComponent,
    PurchaseComponent,
    DeliveredComponent,
    ChangeCategoryComponent,
    EmissionsComponent,
    ColumnChartComponent,
    InspirationSliderComponent,
    PaymentsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
