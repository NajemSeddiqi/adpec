import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IToast } from 'src/app/interfaces/IToast';

export enum ToastType {
  Success = 0,
  Info = 1,
  Warning = 2,
  Error = 3
}

type ToastOptions = {
  toastType: ToastType;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class ToastService implements IToast {
  private displayToastSource = new BehaviorSubject<ToastOptions | undefined>(undefined);
  displayToast$ = this.displayToastSource.asObservable();

  constructor() { }

  success(message: string) {
    this.toast({message: message, toastType: ToastType.Success})
  }

  info(message: string) {
    this.toast({message: message, toastType: ToastType.Info})
  }

  warning(message: string) {
    this.toast({message: message, toastType: ToastType.Warning})
  }

  error(message: string) {
    this.toast({message: message, toastType: ToastType.Success})
  }

  private toast(toastOptions: ToastOptions) {
    this.displayToastSource.next(toastOptions);
  }
}
