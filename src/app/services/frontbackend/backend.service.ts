import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { InspirationCategory } from 'src/app/models/Inspiration/InpirationCategory';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { InvoiceCategory } from 'src/app/models/Invoice/InvoiceCategory';
import { InvoiceCategoryType } from 'src/app/models/Invoice/InvoiceCategoryType';
import { InvoiceStatus } from 'src/app/models/Invoice/InvoiceStatus';
import { InvoiceType } from 'src/app/models/Invoice/InvoiceType';
import { PaymentMethod } from 'src/app/models/Payment/PaymentMethod';
import { Dimensions } from 'src/app/models/Product/Dimensions';
import { ProductDelivery } from 'src/app/models/Product/ProductDelivery';
import { ProductDeliveryCarrier } from 'src/app/models/Product/ProductDeliveryCarrier';
import { weightUnit } from 'src/app/models/Product/WeightUnit';

@Injectable({
  providedIn: 'root',
})
export class BackendService {
  private underTransportationInfo: string = 'The shipment item is under transportation.';
  private atRecipientsDoorInfo: string = `The shipment item has been delivered at the recipient's door.`;
  private jpeg: string = 'jpeg';
  private png: string = 'png';

  private usernameSource = new BehaviorSubject<string>(this.getUsernameFromLocalStorage());
  username$ = this.usernameSource.asObservable();

  invoices$: Observable<Invoice[]> = new Observable(sub => sub.next(this.invoices()));
  categories$: Observable<InvoiceCategory[]> = new Observable(sub => sub.next(this.categories()));
  favoriteInspirations$: Observable<InspirationCategory[]> = new Observable(sub => sub.next(this.favoriteInspirations()));
  exploreInspirations$: Observable<InspirationCategory[]> = new Observable(sub => sub.next(this.exploreInspirations()));
  adpecMain$: Observable<Record<string, string>[]> = new Observable(sub => sub.next(this.adpecMain()));
  adpecMisc$: Observable<Record<string, string>[]> = new Observable(sub => sub.next(this.adpecMisc()));

  constructor() {}

  getInvoice(id: number): Observable<Invoice> {
    return new Observable(sub => sub.next(this.invoices().find((invoice) => invoice.id === id)));
  }

  changeUsername(username: string): void {
    localStorage.setItem('Username', username);
    this.usernameSource.next(username);
  }

  private getUsernameFromLocalStorage(): string {
    const username: string | null = localStorage.getItem('Username');
    return username ?? 'User';
  }

  private invoices(): Invoice[] {
    return [
      {
        id: 2246755,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(2246755),
        company: {
          name: 'Intersport',
          logoPath: this.getAssetPath('Intersport/logo', this.png),
          website: 'https://www.intersport.se/',
        },
        dueDate: this.date(30),
        status: InvoiceStatus.Unpaid,
        tracksPurchaseEmission: true,
        emission: 11.21,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 3086374582,
        storeOrderReference: 67877424,
        purchaseDate: this.date(),
        delivery: undefined,
        products: [
          {
            sum: 449.0,
            title: 'ENE Murph 6 M',
            picturePath: this.getAssetPath(
              'Intersport/ene-murph-6-m',
              this.jpeg
            ),
          },
        ],
      },
      {
        id: 1816232,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(1816232),
        company: {
          name: 'TTEX',
          logoPath: this.getAssetPath('TTEX/logo', this.png),
          website: 'https://www.ttex.se/',
        },
        dueDate: new Date(2023, 3, 27),
        status: InvoiceStatus.Paid,
        tracksPurchaseEmission: true,
        emission: 40.56,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 5802328718,
        storeOrderReference: 15245228,
        purchaseDate: new Date(2023, 2, 27),
        delivery: new ProductDelivery(
          'AD44762535SE',
          new Dimensions('16 cm x 12 cm x 6 cm', 400, weightUnit.g),
          ProductDeliveryCarrier.Postnord,
          [
            {
              information:
                'We have received a notification from your shipper that they are preparing an item for you. The tracking information will be updated when the parcel is handed over to PostNord.',
              location: 'Postnord',
              receivedAt: new Date(2022, 1, 20, 12, 49),
            },
            {
              information: this.underTransportationInfo,
              location: 'Rosersberg BT',
              receivedAt: new Date(2022, 1, 23, 10, 6),
            },
            {
              information: this.underTransportationInfo,
              location: 'Årsta BT',
              receivedAt: new Date(2022, 1, 24, 24, 5),
            },
            {
              information: this.atRecipientsDoorInfo,
              location: 'Stockholm V. Järnvägsg',
              receivedAt: new Date(2022, 1, 25, 12, 4),
            },
          ]
        ),
        products: [
          {
            sum: 499.0,
            title: 'Yasaka Ma Lin Extra Offensive',
            picturePath: this.getAssetPath(
              'TTEX/yasaka-ma-lin-extra-offensive',
              this.jpeg
            ),
          },
          {
            sum: 399.0,
            title: 'Butterfly Rozena Red',
            picturePath: this.getAssetPath('TTEX/butterfly-rozena', this.jpeg),
          },
          {
            sum: 399.0,
            title: 'Butterfly Rozena Black',
            picturePath: this.getAssetPath('TTEX/butterfly-rozena', this.jpeg),
          },
        ],
      },
      {
        id: 5344659,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(5344659),
        company: {
          name: 'Elgigaten',
          logoPath: this.getAssetPath('Elgiganten/logo', this.png),
          website: 'https://www.elgiganten.se/',
        },
        dueDate: this.date(10),
        status: InvoiceStatus.Unpaid,
        tracksPurchaseEmission: true,
        emission: 16.26,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 3088767253,
        storeOrderReference: 61461196,
        purchaseDate: this.date(-20),
        delivery: undefined,
        products: [
          {
            sum: 555.0,
            title: 'Logik element L20CT22E',
            picturePath: this.getAssetPath(
              'Elgiganten/logik-element',
              this.jpeg
            ),
          },
        ],
      },
      {
        id: 2088750,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(2088750),
        company: {
          name: 'Nudient',
          logoPath: this.getAssetPath('Nudient/logo', this.png),
          website: 'https://www.nudient.se/',
        },
        dueDate: this.date(-90),
        status: InvoiceStatus.Paid,
        tracksPurchaseEmission: true,
        emission: 6.3,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 3257833592,
        storeOrderReference: 88324264,
        purchaseDate: this.date(-120),
        delivery: undefined,
        products: [
          {
            sum: 279.3,
            title: 'Nudient Bold Case - Maya Blue - iPhone 12 Pro Max Maya...',
            picturePath: this.getAssetPath(
              'Nudient/nudient-bold-case-maya-blue',
              this.png
            ),
          },
        ],
      },
      {
        id: 1529382,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(1529382),
        company: {
          name: 'Babyland',
          logoPath: this.getAssetPath('Babyland/logo', this.png),
          website: 'https://www.babyland.se/',
        },
        dueDate: new Date(2022, 12, 1),
        status: InvoiceStatus.Paid,
        tracksPurchaseEmission: true,
        emission: 6.78,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 1837813274,
        storeOrderReference: 39463402,
        purchaseDate: new Date(2022, 11, 1),
        delivery: new ProductDelivery(
          'AA97161476SE',
          new Dimensions('289 mm x 202 mm x 377 mm', 500, weightUnit.g),
          ProductDeliveryCarrier.Instabox,
          [
            {
              information: 'Shipment is pending',
              location: 'Babyland',
              receivedAt: new Date(2022, 11, 2, 8, 56),
            },
            {
              information: 'On its way',
              location: 'nstabox DC Stockholm',
              receivedAt: new Date(2022, 11, 2, 12, 6),
            },
            {
              information: 'Ready for pickup',
              location: 'Ica Maxi Stormarknad',
              receivedAt: new Date(2022, 11, 2, 14, 22),
            },
            {
              information: 'Delivered',
              location: 'Ica Maxi Stormarknad',
              receivedAt: new Date(2022, 11, 3, 18, 36),
            },
          ]
        ),
        products: [
          {
            sum: 65.0,
            title: 'Oopsy Schamporing (Rosa)',
            picturePath: this.getAssetPath(
              'Babyland/schamporing-pink',
              this.jpeg
            ),
          },
          {
            sum: 235.0,
            title: 'Skip Hop Stroller Organizer (Svart)',
            picturePath: this.getAssetPath(
              'Babyland/skip-hop-stroller-organizer',
              this.png
            ),
          },
        ],
      },
      {
        id: 159974,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(159974),
        company: {
          name: 'Gymshark',
          logoPath: this.getAssetPath('Gymshark/logo', this.png),
          website: 'https://se.shop.gymshark.com/',
        },
        dueDate: new Date(2022, 8, 7),
        status: InvoiceStatus.Paid,
        tracksPurchaseEmission: true,
        emission: 16.48,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 2344145561,
        storeOrderReference: 97537631,
        purchaseDate: new Date(2022, 7, 8),
        delivery: new ProductDelivery(
          'DO93514988SE',
          new Dimensions('66 cm x 41 cm x 3 cm', 900, weightUnit.g),
          ProductDeliveryCarrier.Postnord,
          [
            {
              information: `The shipment item has been delivered to a service point.`,
              location: 'DIREKTEN',
              receivedAt: new Date(2022, 7, 13, 9, 26),
            },
            {
              information: 'The shipment item has been delivered',
              location: 'DIREKTEN',
              receivedAt: new Date(2022, 7, 19, 17),
            },
          ]
        ),
        products: [
          {
            sum: 449.0,
            title: 'Gymshark Aspect 2 In 1 Short - Black - Medium',
            picturePath: this.getAssetPath(
              'Gymshark/aspect-2-in-1-shorts',
              this.jpeg
            ),
          },
          {
            sum: 399.0,
            title: 'Gymshark Barrel Bag - Black - One Size',
            picturePath: this.getAssetPath(
              'Gymshark/barrel-bag-black-one-size',
              this.jpeg
            ),
          },
        ],
      },
      {
        id: 275377,
        type: InvoiceType.Invoice,
        category: this.getCategoryFromLocalStorageIfExists(275377),
        company: {
          name: 'Kjell & Company',
          logoPath: this.getAssetPath('KjellAndCompany/logo', this.png),
          website: 'https://www.kjell.com/se',
        },
        dueDate: new Date(2022, 3, 1),
        status: InvoiceStatus.Paid,
        tracksPurchaseEmission: true,
        emission: 2.31,
        paymentMethod: PaymentMethod.PayLater,
        paymentReference: 8625767457,
        storeOrderReference: 71592513,
        purchaseDate: new Date(2022, 2, 2),
        delivery: new ProductDelivery(
          'US05283478SE',
          new Dimensions('28 cm x 20 cm x 2 cm', 70, weightUnit.g),
          ProductDeliveryCarrier.Postnord,
          [
            {
              information: `We have received a notification from your shipper that they are preparing an item for you. The tracking information will be updated when the parcel is handed over to PostNord.`,
              location: 'PostNord',
              receivedAt: new Date(2022, 2, 2, 10, 5),
            },
            {
              information: this.underTransportationInfo,
              location: 'Malmö BT',
              receivedAt: new Date(2022, 2, 2, 17, 52),
            },
            {
              information: this.underTransportationInfo,
              location: 'Årsta BT',
              receivedAt: new Date(2022, 2, 3, 3, 45),
            },
            {
              information: `The shipment item has been delivered to the recipient's mailbox.`,
              location: 'Sweden',
              receivedAt: new Date(2022, 2, 3, 10, 50),
            },
          ]
        ),
        products: [
          {
            sum: 79.9,
            title: 'USB-B-Cable Gray 0,5 m',
            picturePath: this.getAssetPath(
              'KjellAndCompany/usb-b-cable-gray',
              this.jpeg
            ),
          },
        ],
      },
    ];
  }

  private category(type: InvoiceCategoryType): InvoiceCategory {
    const category = this.categories().find((category) => category.type === type);

    return category ? category : new InvoiceCategory(InvoiceCategoryType.Other,this.getAssetPath('/Icons/category/other', this.png));
  }

  private categories(): InvoiceCategory[] {
    return [
      new InvoiceCategory(
        InvoiceCategoryType.Clothing,
        this.getAssetPath('/Icons/category/clothing', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Electronic,
        this.getAssetPath('/Icons/category/electronics', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Home,
        this.getAssetPath('/Icons/category/home', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.FoodAndDrinks,
        this.getAssetPath('/Icons/category/food-and-drinks', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Leisure,
        this.getAssetPath('/Icons/category/leisure', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.SelfCare,
        this.getAssetPath('/Icons/category/self-care', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Family,
        this.getAssetPath('/Icons/category/family', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Other,
        this.getAssetPath('/Icons/category/other', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Transport,
        this.getAssetPath('/Icons/category/transport', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Travel,
        this.getAssetPath('/Icons/category/travel', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Giving,
        this.getAssetPath('/Icons/category/giving', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Bills,
        this.getAssetPath('/Icons/category/bills', this.png)
      ),
      new InvoiceCategory(
        InvoiceCategoryType.Exercise,
        this.getAssetPath('/Icons/category/exercise', this.png)
      ),
    ];
  }

  private getCategoryFromLocalStorageIfExists(id: number): InvoiceCategory {
    const category = localStorage.getItem(id.toString());

    if (!category) {
      switch (id) {
        case 2246755:
          return this.category(InvoiceCategoryType.Clothing);
        case 1816232:
          return this.category(InvoiceCategoryType.Exercise);
        case 5344659:
          return this.category(InvoiceCategoryType.Electronic);
        case 2088750:
          return this.category(InvoiceCategoryType.Clothing);
        case 1529382:
          return this.category(InvoiceCategoryType.Family);
        case 159974:
          return this.category(InvoiceCategoryType.Clothing);
        case 275377:
          return this.category(InvoiceCategoryType.Electronic);
        default:
          return new InvoiceCategory(
            InvoiceCategoryType.Other,
            this.getAssetPath('/Icons/category/other', this.png)
          );
      }
    }

    return this.category(JSON.parse(category));
  }

  private favoriteInspirations(): InspirationCategory[] {
    const assetPath = (name: string): string =>
      this.getAssetPath(`Inspirations/${name}`, this.png);

    return [
      {
        id: 0,
        title: 'Stay-at-home style',
        marquee: 'Matching sweats',
        iconPath: assetPath('stay-at-home-style'),
      },
      {
        id: 1,
        title: 'Accessories',
        marquee: 'Bling out!',
        iconPath: assetPath('accessories'),
      },
      {
        id: 2,
        title: 'Get better shut eye',
        marquee: 'Sleeping easy',
        iconPath: assetPath('get-better-shut-eye'),
      },
      {
        id: 3,
        title: 'Day gifting guide',
        marquee: 'Best gift picks',
        iconPath: assetPath('day-gifting-guide'),
      },
      {
        id: 4,
        title: 'Top shoes',
        marquee: 'Fresh sneakers',
        iconPath: assetPath('top-shoes'),
      },
      {
        id: 5,
        title: 'Everything cats love',
        marquee: 'Feline friends',
        iconPath: assetPath('everything-cats-love'),
      },
      {
        id: 6,
        title: 'Streetwear',
        marquee: 'Most hyped',
        iconPath: assetPath('streetwear'),
      },
    ];
  }

  private exploreInspirations(): InspirationCategory[] {
    const assetPath = (name: string): string =>
      this.getAssetPath(`Inspirations/${name}`, this.png);

    return [
      {
        id: 7,
        title: 'Game on',
        marquee: 'Gaming',
        iconPath: assetPath('game-on'),
      },
      {
        id: 8,
        title: `Let's get technical`,
        marquee: 'Tech',
        iconPath: assetPath('lets-get-technical'),
      },
      {
        id: 9,
        title: 'Wellness esssentials',
        marquee: 'Wellness',
        iconPath: assetPath('wellness-essentials'),
      },
      {
        id: 10,
        title: 'New to the job',
        marquee: 'New parents',
        iconPath: assetPath('new-to-the-job'),
      },
      {
        id: 11,
        title: 'Beauty essentials',
        marquee: 'Beauty',
        iconPath: assetPath('beauty-essentials'),
      },
    ];
  }

  private adpecMain(): Record<string, string>[] {
    return [
      {title: "Payments", iconPath: this.adpecIconPath("payments")},
      {title: "Purchase Power", iconPath: this.adpecIconPath("purchase-power")},
      {title: "Orders", iconPath: this.adpecIconPath("orders")},
    ];
  }

  private adpecMisc(): Record<string, string>[] {
    return [
      {title: "Part Pay", iconPath: this.adpecIconPath("part-pay")},
      {title: "My Stores", iconPath: this.adpecIconPath("my-stores")},
      {title: "Payment Methods", iconPath: this.adpecIconPath("payment-methods")},
      {title: "Impact", iconPath: this.getAssetPath("/Icons/emission-leaf", this.png)},
      {title: "Klarna Card", iconPath: this.adpecIconPath("klarna-card")},
      {title: "Automatic Payments", iconPath: this.adpecIconPath("automatic-payments")},
      {title: "Customer Service", iconPath: this.adpecIconPath("customer-service")},
    ];
  }

  private adpecIconPath(name: string): string {
    return this.getAssetPath(`/Icons/my-adpec/${name}`, this.png);
  }

  private date(additionalDays: number = 0): Date {
    let date = new Date();
    date.setDate(date.getDate() + additionalDays);
    return date;
  }

  private getAssetPath(relativePath: string, fileType: string): string {
    const assetPath: string = '../../assets';
    return `${assetPath}/${relativePath}.${fileType}`;
  }
}
