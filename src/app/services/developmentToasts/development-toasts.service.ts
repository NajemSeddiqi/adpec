import { Injectable } from '@angular/core';
import { ToastService } from '../toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class DevelopmentToastsService {

  constructor(private toast: ToastService) { }

  displayNoFunctionalityToast(event: Event) {
    event.stopPropagation();
    this.toast.info("This is simply a cosmetic site with no real backend functionality :)");
  }
}
