import { TestBed } from '@angular/core/testing';

import { DevelopmentToastsService } from './development-toasts.service';

describe('DevelopmentToastsService', () => {
  let service: DevelopmentToastsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DevelopmentToastsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
