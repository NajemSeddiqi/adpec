export type Company = {
  name: string;
  logoPath: string;
  website: string;
}
