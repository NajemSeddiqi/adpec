export type InspirationCategory = {
  id: number;
  title: string;
  marquee: string;
  iconPath: string;
}
