export enum PaymentMethod {
  PayLater = 0,
  PayNow = 1,
}
