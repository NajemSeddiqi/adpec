import { weightUnit } from "./WeightUnit";

export class Dimensions {
  size: string;
  private weight: number;
  private weightUnit: weightUnit;

  constructor(size: string, weight: number, weightUnit: weightUnit) {
    this.size = size;
    this.weight = weight;
    this.weightUnit = weightUnit
  }

  getWeightWithUnit(): string {
    return `${this.weight} ${this.weightUnit}`;
  }
}
