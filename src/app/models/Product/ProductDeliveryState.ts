export type ProductDeliveryState = {
  location: string;
  receivedAt: Date;
  information: string;
}
