export enum ProductDeliveryCarrier {
  Instabox = 0,
  Postnord = 1,
  Budbee = 2,
}
