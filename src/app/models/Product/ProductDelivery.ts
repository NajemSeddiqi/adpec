import { Dimensions } from "./Dimensions";
import { ProductDeliveryCarrier } from "./ProductDeliveryCarrier";
import { ProductDeliveryState } from "./ProductDeliveryState";

export class ProductDelivery  {
  trackingNumber: string;
  dimensions: Dimensions;
  private carrier: ProductDeliveryCarrier;
  private states: ProductDeliveryState[];

  constructor(trackingNumber: string, dimensions: Dimensions, carrier: ProductDeliveryCarrier, states: ProductDeliveryState[]) {
      this.trackingNumber = trackingNumber;
      this.dimensions = dimensions;
      this.carrier = carrier;
      this.states = states;
  }

  public getStates(): ProductDeliveryState[] {
    return this.states.sort((a, b) => b.receivedAt.getTime() - a.receivedAt.getTime());
  }

  getCarrierName(): string {
    return ProductDeliveryCarrier[this.carrier];
  }
}
