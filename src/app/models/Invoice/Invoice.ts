import { InvoiceCategoryType } from './InvoiceCategoryType';
import { InvoiceProduct } from './InvoiceProduct';
import { InvoiceStatus } from './InvoiceStatus';
import { InvoiceType } from './InvoiceType';
import { PaymentMethod } from '../Payment/PaymentMethod';
import { ProductDelivery } from '../Product/ProductDelivery';
import { Company } from '../Company/Company';
import { InvoiceCategory } from './InvoiceCategory';

export type Invoice = {
  id: number;
  type: InvoiceType;
  company: Company;
  tracksPurchaseEmission: boolean;
  emission: number;
  status: InvoiceStatus;
  category: InvoiceCategory;
  purchaseDate: Date;
  dueDate: Date;
  paymentMethod: PaymentMethod;
  paymentReference: number;
  storeOrderReference: number;
  products: InvoiceProduct[];
  delivery: ProductDelivery | undefined;
};
