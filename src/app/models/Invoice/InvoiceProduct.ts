export type InvoiceProduct = {
  title: string;
  sum: number;
  picturePath: string;
}
