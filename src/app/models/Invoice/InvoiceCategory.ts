import { InvoiceCategoryType } from "./InvoiceCategoryType"

export class InvoiceCategory  {
  type: InvoiceCategoryType;
  iconPath: string;

  constructor(type: InvoiceCategoryType, iconPath: string) {
    this.type = type;
    this.iconPath = iconPath;
  }

  getTitle(): string {
    const parsedTitle = InvoiceCategoryType[this.type].replace(/([a-z])([A-Z])/g, "$1 $2");
    return parsedTitle;
  }

}
