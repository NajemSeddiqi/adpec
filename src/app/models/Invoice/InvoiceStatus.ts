export enum InvoiceStatus {
  Paid = 0,
  Unpaid = 1
}
