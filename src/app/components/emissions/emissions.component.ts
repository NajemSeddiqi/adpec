import { Component, OnDestroy, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-emissions',
  templateUrl: './emissions.component.html',
  styleUrls: ['./emissions.component.scss'],
})
export class EmissionsComponent
  extends DisposableComponentBase
  implements OnInit, OnDestroy
{
  invoices: Invoice[] = [];
  xAxisLabels: string[] = [];
  yAxisLabels: number[] = [];
  pastTwelveMonths: [string, { logoPath: string; totalEmissions: number }][] =
    [];

  constructor(private api: BackendService) {
    super();
  }

  ngOnInit(): void {
    this.registerDisposable(this.api.invoices$.pipe(tap((next) => (this.invoices = next))).subscribe());
    this.setColumnChartLabels();
    this.setPastTwelveMonths();
  }

  setColumnChartLabels() {
    const labels = this.invoices
      .filter((invoice) => invoice.tracksPurchaseEmission)
      .sort((a, b) => a.purchaseDate.getMonth() - b.purchaseDate.getMonth())
      .asDictionary(
        (invoice) => invoice.emission,
        (invoice) => invoice.purchaseDate.getShortMonthName(),
        (record, key, value) => (record[key] = Number(value.toFixed(2))),
        (record, key, value) => (record[key] += Number(value.toFixed(1)))
      );

    this.xAxisLabels = labels[0];
    this.yAxisLabels = labels[1];
  }

  setPastTwelveMonths() {
    const currentDate = new Date();
    const twelveMonthsAgo = new Date();
    twelveMonthsAgo.setFullYear(currentDate.getFullYear() - 1);

    let pastTwelveMonths: Record<string, { logoPath: string; totalEmissions: number }> = {};

    this.invoices
      .filter( (invoice) => invoice.purchaseDate >= twelveMonthsAgo && invoice.purchaseDate <= currentDate)
      .forEach((invoice) => {
        const company = invoice.company;
        const emission = invoice.emission;

        if (pastTwelveMonths[company.name]) pastTwelveMonths[company.name].totalEmissions += Number(emission.toFixed(1));
        else pastTwelveMonths[company.name] = { logoPath: company.logoPath, totalEmissions: Number(emission.toFixed(1))};
      });

    if (Object.keys(pastTwelveMonths).length > 3)
      this.pastTwelveMonths = Object.entries(pastTwelveMonths).sort((a, b) => b[1].totalEmissions - a[1].totalEmissions).slice(0, 3);
    else
      this.pastTwelveMonths = Object.entries(pastTwelveMonths).sort((a, b) => b[1].totalEmissions - a[1].totalEmissions);
  }

  ngOnDestroy(): void {
    this.dispose();
  }
}
