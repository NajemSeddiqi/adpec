import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { DevelopmentToastsService } from 'src/app/services/developmentToasts/development-toasts.service';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-my-adpec',
  templateUrl: './my-adpec.component.html',
  styleUrls: ['./my-adpec.component.scss']
})
export class MyAdpecComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  username: string = "";

  main: Record<string, string>[] = [];
  misc: Record<string, string>[] = [];

  underDevelopment: string[] = ["Purchase Power", "Part Pay", "Payment Methods", "Klarna Card", "Automatic Payments", "Settings"];
  goToComponentUrls!: Map<string, string>;

  openModal: boolean = false;

  constructor(private api: BackendService, private devToast: DevelopmentToastsService, private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.registerDisposable(this.api.adpecMain$.pipe(tap(next => this.main = next)).subscribe());
    this.registerDisposable(this.api.adpecMisc$.pipe(tap(next => this.misc = next)).subscribe());
    this.registerDisposable(this.api.username$.pipe(tap(next => this.username = next)).subscribe());

    this.goToComponentUrls = new Map<string, string>([
      ["Payments", "/payments"],
      ["Orders", "/purchases"],
      ["Impact", "/emissions"],
      ["My Stores", "/myStores"],
      ["Customer Service", "/service"],
    ]);
  }

  goTo(title: string, event: Event): void {
    if(this.underDevelopment.includes(title)) {
      this.devToast.displayNoFunctionalityToast(event);
    } else {
      this.router.navigate([this.goToComponentUrls.get(title)]);
    }
  }

  downloadKlarnaApp(): void {
    window.open('https://www.klarna.com/us/klarna-app/', '_blank');
  }

  openUser(): void {
    this.openModal = !this.openModal;
  }

  changeUsername(username: string): void {
    this.api.changeUsername(username);
    this.openModal = !this.openModal;
  }

  ngOnDestroy(): void {
    this.dispose();
  }
}
