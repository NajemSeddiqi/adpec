import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAdpecComponent } from './my-adpec.component';

describe('MyAdpecComponent', () => {
  let component: MyAdpecComponent;
  let fixture: ComponentFixture<MyAdpecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyAdpecComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyAdpecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
