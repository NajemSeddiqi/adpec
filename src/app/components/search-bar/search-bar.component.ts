import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Invoice } from 'src/app/models/Invoice/Invoice';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Input() orders: Invoice[] = [];

  inputValue: string = "";
  searching: boolean = false;
  invoices: Invoice[] = [];

  constructor(private router: Router) { }

  ngOnInit(): void {}

  onInputKey(event: any) {
    if(event.target.value !== "") {
      this.searching = true;
      this.invoices = [];
    }

    this.inputValue = event.target.value.toLowerCase();

    setTimeout(() => {
      if(this.inputValue !== "") {
          this.invoices = this.orders.filter(invoice => invoice.company.name.toLowerCase().includes(this.inputValue));
          this.searching = false;
      } else {
        this.invoices = [];
        this.searching = false;
      }
    }, 1000);
  }

  goToOrder(id: number) {
    console.log(id);
    this.router.navigate(['/purchase', id]);
  }

  clearInput() {
    this.inputValue = "";
  }

}
