import { Component, OnDestroy, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { DevelopmentToastsService } from 'src/app/services/developmentToasts/development-toasts.service';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-bank-and-card',
  templateUrl: './bank-and-card.component.html',
  styleUrls: ['./bank-and-card.component.scss']
})
export class BankAndCardComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  invoices: Invoice[] = [];
  xAxisLabels: string[] = [];
  yAxisLabels: number[] = [];

  constructor(private api: BackendService, private devToast: DevelopmentToastsService) { super(); }

  ngOnInit(): void {
    this.registerDisposable(this.api.invoices$.pipe(tap((next) => (this.invoices = next))).subscribe());
    this.setColumnChartLabels();
  }

  setColumnChartLabels() {
    const labels = this.invoices
      .sort((a, b) => a.purchaseDate.getMonth() - b.purchaseDate.getMonth())
      .asDictionary(
        invoice => invoice.products.reduce((a, b) => a + b.sum, 0),
        invoice => invoice.purchaseDate.getShortMonthName(),
        (record, key, value) => (record[key] = Number(value.toFixed(2))),
        (record, key, value) => (record[key] += Number(value.toFixed(1)))
      );

    this.xAxisLabels = labels[0];
    this.yAxisLabels = labels[1];
  }

  displayDevToast(event: Event) {
    this.devToast.displayNoFunctionalityToast(event);
  }

  ngOnDestroy(): void {
    this.dispose();
  }
}
