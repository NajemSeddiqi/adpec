import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankAndCardComponent } from './bank-and-card.component';

describe('BankAndCardComponent', () => {
  let component: BankAndCardComponent;
  let fixture: ComponentFixture<BankAndCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankAndCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BankAndCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
