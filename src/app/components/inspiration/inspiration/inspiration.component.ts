import { Component, OnDestroy, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { InspirationCategory } from 'src/app/models/Inspiration/InpirationCategory';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-inspiration',
  templateUrl: './inspiration.component.html',
  styleUrls: ['./inspiration.component.scss']
})
export class InspirationComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  favorites: InspirationCategory[] = [];
  explore: InspirationCategory[] = [];

  constructor(private api: BackendService) { super(); }

  ngOnInit(): void {
    this.registerDisposable(this.api.favoriteInspirations$.pipe(tap(next => {this.favorites = next;})).subscribe());
    this.registerDisposable(this.api.exploreInspirations$.pipe(tap(next => this.explore = next)).subscribe());
  }

  ngOnDestroy(): void {
    this.dispose();
  }
}
