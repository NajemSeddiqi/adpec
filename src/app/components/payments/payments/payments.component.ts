import { Component, OnDestroy, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { InvoiceStatus } from 'src/app/models/Invoice/InvoiceStatus';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  paidInvoices: Invoice[] = [];
  unpaidInvoices: Invoice[] = [];

  invoices: Invoice[] = [];
  tab: number = 0;

  constructor(private api: BackendService) { super(); }

  ngOnInit(): void {
    this.registerDisposable(this.api.invoices$.pipe(tap(next => {
      this.paidInvoices = next.filter(invoice => invoice.status === InvoiceStatus.Paid);
      this.unpaidInvoices = next.filter(invoice => invoice.status === InvoiceStatus.Unpaid);
      this.invoices = this.paidInvoices;
    })).subscribe());
  }

  toggleTab(tab: number): void {
    this.tab = tab;
    if(tab === 0) this.invoices = this.paidInvoices;
    else this.invoices = this.unpaidInvoices;
  }

  ngOnDestroy(): void {
    this.dispose();
  }
}
