import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { InvoiceStatus } from 'src/app/models/Invoice/InvoiceStatus';
import { DevelopmentToastsService } from 'src/app/services/developmentToasts/development-toasts.service';
import { BackendService } from 'src/app/services/frontbackend/backend.service';
import { ToastService } from 'src/app/services/toast/toast.service';

interface IAction {
  [key: number]: () => void;
}

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss'],
})
export class PurchasesComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  invoices: { id: number; items: Invoice[]; isActive: boolean } = {
    id: 0,
    items: [],
    isActive: true,
  };
  onItsWay: { id: number; items: Invoice[]; isActive: boolean } = {
    id: 1,
    items: [],
    isActive: false,
  };
  delivered: { id: number; items: Invoice[]; isActive: boolean } = {
    id: 2,
    items: [],
    isActive: false,
  };

  currentInvoices: Invoice[] = [];


  constructor(private api: BackendService, private router: Router, private devToast: DevelopmentToastsService) {
    super();
  }

  ngOnInit(): void {
    this.registerDisposable(this.api.invoices$.pipe(tap((next) => {
      this.invoices.items = next;
      this.currentInvoices = next.sort((a, b) => b.purchaseDate.getTime() - a.purchaseDate.getTime());
    })).subscribe());

    this.onItsWay = {
      id: this.onItsWay.id,
      items: this.invoices.items.filter((invoice) => invoice.status === InvoiceStatus.Unpaid),
      isActive: false,
    };

    this.delivered = {
      id: this.delivered.id,
      items: this.invoices.items.filter((invoice) => invoice.status === InvoiceStatus.Paid),
      isActive: false,
    };
  }

  toggleTab(id: number) {
    const action: IAction = {
      0: () => {
        this.toggleActiveTab(0);
        this.currentInvoices = this.invoices.items;
      },
      1: () => {
        this.toggleActiveTab(1);
        this.currentInvoices = this.onItsWay.items;
      },
      2: () => {
        this.toggleActiveTab(2);
        this.currentInvoices = this.delivered.items;
      }
    };

    action[id]();
  }

  goToOrder(id: number) {
    this.router.navigate(['/purchase', id]);
  }

  toastAbout(event: Event){
    this.devToast.displayNoFunctionalityToast(event);
  }

  private toggleActiveTab(id: number) {
    this.invoices.isActive = id === 0 ? true : false;
    this.onItsWay.isActive = id === 1 ? true : false;
    this.delivered.isActive = id === 2 ? true : false;
  }

  ngOnDestroy(): void {
    this.dispose();
  }
}
