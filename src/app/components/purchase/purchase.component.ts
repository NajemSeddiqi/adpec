import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { InvoiceCategory } from 'src/app/models/Invoice/InvoiceCategory';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  invoice: Invoice | undefined = undefined;
  productPicturePaths: string[] = [];
  currentPictureIndex: number = 0;
  additionalInfo: {title: string, value: string}[] = [];
  showChangeCategoryModal: boolean = false;

  constructor(private route: ActivatedRoute, private api: BackendService, private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.registerDisposable(
      this.route.params.subscribe(params => {
        this.registerDisposable(this.api.getInvoice(Number(params['id'])).pipe(tap(next => {
          this.invoice = next;
          this.productPicturePaths = next.products.map(product => product.picturePath);
          this.additionalInfo = [
            {title: 'Purchase date', value: this.invoice.purchaseDate.toDateString(),},
            {title: 'Payment status', value: this.invoice.status === 0 ? 'Paid' : 'Unpaid'},
            {title: 'Payment method', value: this.invoice.paymentMethod === 0 ? 'Pay later' : 'Pay now'},
            {title: 'Payment reference', value: this.invoice.paymentReference.toString()},
            {title: 'Store order reference', value: this.invoice.storeOrderReference.toString()}
        ];
        })).subscribe());
      })
    )
  }

  selectPicture(index: number) {
    this.currentPictureIndex = index;
  }

  goToDelivered() {
    this.router.navigate(['/delivered', this.invoice?.id]);
  }

  changeCategory() {
    this.showChangeCategoryModal = !this.showChangeCategoryModal;
    this.ngOnInit();
  }

  goToEmissions() {
    this.router.navigate(['/emissions']);
  }

  ngOnDestroy(): void {
    this.dispose();
  }

}
