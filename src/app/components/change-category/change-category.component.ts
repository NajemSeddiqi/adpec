import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Invoice } from 'src/app/models/Invoice/Invoice';
import { InvoiceCategory } from 'src/app/models/Invoice/InvoiceCategory';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-change-category',
  templateUrl: './change-category.component.html',
  styleUrls: ['./change-category.component.scss']
})
export class ChangeCategoryComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  @Input() invoice?: Invoice = undefined;
  @Output() closeModalEvent = new EventEmitter();

  categories: InvoiceCategory[] = [];

  constructor(private api: BackendService) {
    super();
  }

  ngOnInit(): void {
    this.registerDisposable(this.api.categories$.pipe(tap(next => this.categories = next)).subscribe())
  }

  select(category: InvoiceCategory) {
    if(!this.invoice) return;
    localStorage.setItem(this.invoice.id.toString(), JSON.stringify(category.type));
    this.closeModalEvent.emit();
  }

  close() {
    this.closeModalEvent.emit();
  }

  ngOnDestroy(): void {
    this.dispose();
  }

}
