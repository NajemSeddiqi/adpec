import { Component, Input, OnInit } from '@angular/core';
import { InspirationCategory } from 'src/app/models/Inspiration/InpirationCategory';
import { DevelopmentToastsService } from 'src/app/services/developmentToasts/development-toasts.service';

@Component({
  selector: 'app-inspiration-slider',
  templateUrl: './inspiration-slider.component.html',
  styleUrls: ['./inspiration-slider.component.scss'],
})
export class InspirationSliderComponent implements OnInit {
  @Input() items: InspirationCategory[] = [];

  allItems: {category: InspirationCategory, translateValue: number}[] = [];
  currentDisplayed: {category: InspirationCategory, translateValue: number}[] = [];

  leftIdx: number = 0;
  showLeftBtn: boolean = false;

  slidedLeft: boolean = false;
  slidedRight: boolean = false;

  rightIdx: number = 2;
  showRightBtn: boolean = true;

  constructor(private devToast: DevelopmentToastsService) { }

  ngOnInit(): void {
    this.allItems = this.items.map(cat => ({category: cat, translateValue: 0}));
    this.currentDisplayed = this.allItems.map(item => ({category: item.category, translateValue: item.translateValue}));
  }

  slideRight() {
    this.currentDisplayed.forEach(item => item.translateValue -= 14.3);
    ++this.rightIdx;
    ++this.leftIdx;
    this.slidedRight = true;
    this.slidedLeft = !this.slidedRight;
    this.updateSlideButtons();
  }

  slideLeft() {
    this.currentDisplayed.forEach(item => item.translateValue += 14.3);
    --this.leftIdx;
    --this.rightIdx;
    this.slidedLeft = true;
    this.slidedRight = !this.slideLeft;
    this.updateSlideButtons();
  }

  updateSlideButtons() {
    if(this.leftIdx === 0) this.showLeftBtn = false;
    else this.showLeftBtn = true;

    if(this.rightIdx === this.items.length - 1) this.showRightBtn = false;
    else this.showRightBtn = true;
  }

  openCategory(event: Event) {
    this.devToast.displayNoFunctionalityToast(event);
  }
}
