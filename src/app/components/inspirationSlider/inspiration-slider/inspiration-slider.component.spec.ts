import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspirationSliderComponent } from './inspiration-slider.component';

describe('InspirationSliderComponent', () => {
  let component: InspirationSliderComponent;
  let fixture: ComponentFixture<InspirationSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspirationSliderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InspirationSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
