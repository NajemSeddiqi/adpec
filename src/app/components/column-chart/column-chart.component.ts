import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-column-chart',
  templateUrl: './column-chart.component.html',
  styleUrls: ['./column-chart.component.scss'],
})
export class ColumnChartComponent implements OnInit {
  @Input() xAxisLabels: (string | number)[] = [];
  @Input() yAxisLabels: number[] = [];
  @Input() isEmissionsChart: boolean = true;

  data: { month: string | number; value: number }[] = [];
  highlightedMonth: { month: string | number; value: number } | undefined = undefined;
  avgValue: number = 0;
  maxY: number = -1;

  constructor() {}

  ngOnInit(): void {
    this.setData();
    this.maxY = Math.max(...this.yAxisLabels);
    this.highlightedMonth = this.data[this.data.length-1];
    this.avgValue = Number(((this.data.reduce((a, b) => a + b.value, 0) / this.data.length)).toFixed(1)) || 0;
  }

  highlightMonth(month: string | number) {
    const current = this.data.find(x => x.month === month);
    if(!current) return;

    this.highlightedMonth = {month: current.month, value: current.value};
  }

  setData() {
    for (let i = 0; i < this.yAxisLabels.length; ++i) {
      const emission = this.yAxisLabels[i];
      const month = this.xAxisLabels[i];

      this.data.push({ month, value: emission });
    }
  }
}
