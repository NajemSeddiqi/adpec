import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs';
import { DisposableComponentBase } from 'src/app/core/DisposableComponentBase';
import { Company } from 'src/app/models/Company/Company';
import { ProductDelivery } from 'src/app/models/Product/ProductDelivery';
import { ProductDeliveryState } from 'src/app/models/Product/ProductDeliveryState';
import { BackendService } from 'src/app/services/frontbackend/backend.service';

@Component({
  selector: 'app-delivered',
  templateUrl: './delivered.component.html',
  styleUrls: ['./delivered.component.scss']
})
export class DeliveredComponent extends DisposableComponentBase implements OnInit, OnDestroy {
  delivery: ProductDelivery | undefined = undefined;
  states: ProductDeliveryState[] = [];
  company: Company | undefined = undefined;
  deliveredDate: string = "";

  constructor(private route: ActivatedRoute, private api: BackendService) {
    super();
  }

  ngOnInit(): void {
    this.registerDisposable(
      this.route.params.subscribe(params =>
        this.registerDisposable(
          this.api.getInvoice(Number(params['id'])).pipe(tap(next => {
            if(!next || !next.delivery) return;
            this.delivery = next.delivery;
            this.states = next.delivery.getStates();
            this.company = next.company;
            const arrivedDate = next.delivery?.getStates()[0].receivedAt;
            this.deliveredDate = `${arrivedDate?.getLongMonthName()} ${arrivedDate?.getDate()}, ${arrivedDate?.getFullYear()}`;
          })).subscribe()
        )
      )
    )
  }

  ngOnDestroy(): void {
    this.dispose();
  }

}
