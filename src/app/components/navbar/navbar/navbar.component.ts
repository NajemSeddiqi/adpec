import { Component, OnInit } from '@angular/core';
import { Routes} from '@angular/router';
import { routes } from 'src/app/routes';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  routes: Routes = [];

  constructor() {}

  ngOnInit(): void {
    this.routes = routes.filter(route => route.path !== "")
    .filter(route => route.path !== 'purchase/:id')
    .filter(route => route.path !== 'delivered/:id')
    .filter(route => route.path !== 'emissions');
  }

}
